Corporate events
-----------------------

Adds a block of events in event detail page.
This block will display 3 more events related to the current event displayed.

Information for users
---------------------

Please, log in the admin interface then access to the layout block page: /admin/structure/block
Finally, you just add to add the bloc name 'Related Events Blocks' then save.

Usefull commands
---------------------

To execute the cron of this module:
  ./vendor/bin/drush eval "corporate_events_cron();"

To run queue that unpublishs expired events:
  ./vendor/bin/drush queue:run unpublish_expired_events
