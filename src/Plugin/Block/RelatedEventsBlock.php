<?php

namespace Drupal\corporate_events\Plugin\Block;

use Drupal\corporate_events\Manager\EventManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that displays related events.
 *
 * @Block(
 *   id = "related_events_block",
 *   admin_label = @Translation("Related events block"),
 *   category = @Translation("Content"),
 * )
 */
class RelatedEventsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * An entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The entity view builder interface.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The event manager interface.
   *
   * @var \Drupal\corporate_events\Manager\EventManagerInterface
   */
  protected $eventManager;

  /**
   * Constructs a RelatedEventsBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory object.
   * @param \Drupal\corporate_events\Manager\EventManagerInterface $event_manager
   *   The event manager object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_channel_factory, EventManagerInterface $event_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->eventManager = $event_manager;
    $this->loggerChannelFactory = $logger_channel_factory;

    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    $this->node = $route_match->getParameter('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('corporate_events.event_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    if (!$this->node instanceof NodeInterface || $this->node->bundle() !== EventManagerInterface::CONTENT_TYPE) {
      return $build;
    }

    try {
      $nids = $this->getRelatedEvents();
      $events = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

      return $this->viewBuilder->viewMultiple($events, 'teaser');
    }
    catch (\Exception $e) {
      $this->loggerChannelFactory->get('corporate_events')
        ->error('An exception occurred: @error', [
          '@error' => $e->getMessage(),
        ]);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Every new url this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // With this when the current node change your block will rebuild.
    $default_cache_tags = Cache::mergeTags(parent::getCacheTags(), ['node_list:event']);
    if (!empty($this->node)) {
      // If there is node add its cache tag.
      return Cache::mergeTags($default_cache_tags, $this->node->getCacheTags());
    }

    // Return default tags instead.
    return $default_cache_tags;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account, $return_as_object = FALSE) {
    if (empty($this->node)) {
      return AccessResult::allowed();
    }
    return $this->node->access('view', NULL, TRUE);
  }

  /**
   * Load the related events based on current event.
   *
   * @return array
   *   Returns an array of ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getRelatedEvents() :array {
    $nids = [];
    // To not repeat the current event in the block results.
    $excluded_nids = [$this->node->id()];

    if ($this->node->hasField('field_event_type') && !$this->node->get('field_event_type')->isEmpty()) {
      /** @var \Drupal\taxonomy\TermInterface[] $term */
      $term = $this->node->get('field_event_type')->referencedEntities();
      if (!empty($term = reset($term))) {
        $nids = $this->eventManager->getRelativeEvents($term, $excluded_nids);
      }
    }

    // Load the remaining events if the max number of events is not reached.
    $remaining_nids_count = EventManagerInterface::MAX_EVENTS - count($nids);
    if ($remaining_nids_count > 0) {
      // To not repeat the events already loaded in the block results.
      $excluded_nids = array_merge($excluded_nids, array_values($nids));
      $remaining_nids = $this->eventManager->getRelativeEvents(NULL, $excluded_nids, $remaining_nids_count);
      $nids += $remaining_nids;
    }

    return $nids;
  }

}
