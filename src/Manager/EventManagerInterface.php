<?php

namespace Drupal\corporate_events\Manager;

use Drupal\taxonomy\TermInterface;

/**
 * Event Manager interface.
 */
interface EventManagerInterface {
  const CONTENT_TYPE = 'event';
  const MAX_EVENTS = 3;

  /**
   * To get expired events ids.
   *
   * @return array|int
   *   Return an array of ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getExpiredEvents(): array|int;

  /**
   * To get relative events based on term.
   *
   * @param \Drupal\taxonomy\TermInterface|null $term
   *   The term object to filter event results.
   * @param array|null $nids
   *   An array of ids to exclude from results.
   * @param int $limit
   *   Set the maximum of results to return.
   *
   * @return int|array
   *   Return an array of ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRelativeEvents(?TermInterface $term, ?array $nids, int $limit = self::MAX_EVENTS): array|int;

}
