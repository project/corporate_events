<?php

namespace Drupal\corporate_events\Manager;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Event Manager class.
 */
class EventManager implements EventManagerInterface {

  /**
   * An entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs an event manager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelativeEvents(?TermInterface $term, ?array $nids, int $limit = self::MAX_EVENTS): array|int {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $date = $this->getCurrentDateTime();

    $query
      ->accessCheck()
      ->condition('type', self::CONTENT_TYPE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('field_date_range.end_value', $date, '>=')
      ->sort('field_date_range.value', 'asc');

    if ($term) {
      $query->condition('field_event_type.target_id', $term->id());
    }
    // To prevent to repeat the current event in the events block.
    if (!empty($nids)) {
      $query->condition('nid', $nids, 'NOT IN');
    }
    $query->range(0, $limit);

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getExpiredEvents(): array|int {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $date = $this->getCurrentDateTime();

    $query
      ->accessCheck()
      ->condition('type', self::CONTENT_TYPE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('field_date_range.end_value', $date, '<');

    return $query->execute();
  }

  /**
   * Get the current date and time formatted as a string.
   *
   * @return string
   *   Return the value of the current date time in string.
   */
  private function getCurrentDateTime(): string {
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));

    return $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

}
